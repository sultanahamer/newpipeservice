FROM openjdk:11-jdk-slim AS builder
ADD . /app
WORKDIR /app
RUN ./gradlew bootJar

FROM openjdk:11-jre-slim
COPY --from=builder /app/build/libs/newpipeservice-0.0.1-SNAPSHOT.jar ./target.jar
CMD ["java", "-jar", "./target.jar"]
