## Usage
Download the latest build from link below. Double click the jar file downloaded.
This file runs in background and doesn't show up anything in front.

After this, open browser and visit the link https://sultanahamer.gitlab.io/newpipeui/

Enjoy! Or in case things don't work, raise an issue.

## Latest build

You can find latest build from [here](https://gitlab.com/sultanahamer/newpipeservice/-/jobs/artifacts/master/raw/build/libs/newpipeservice-0.0.1-SNAPSHOT.jar?job=build)

## Build

```
./gradlew bootJar
```

This creates jar in directory build/libs/****.jar. 