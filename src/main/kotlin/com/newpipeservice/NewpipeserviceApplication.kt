package com.newpipeservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class NewpipeserviceApplication

fun main(args: Array<String>) {
	runApplication<NewpipeserviceApplication>(*args)
}
