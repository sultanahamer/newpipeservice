package com.newpipeservice.model

import org.schabi.newpipe.extractor.InfoItem
import org.schabi.newpipe.extractor.stream.SubtitlesStream
import org.schabi.newpipe.extractor.stream.VideoStream
import org.schabi.newpipe.extractor.stream.AudioStream
import org.schabi.newpipe.extractor.stream.Frameset

data class YTVideoDetails(
        val title: String,
        val description: String,
        val videoAndAudioStreams: List<VideoStream>,
        val videoStreams: List<VideoStream>,
        val audioStreams: List<AudioStream>,
        val subtitleStreams: List<SubtitlesStream>,
        val uploadedDate: String?,
        val likes: Long,
        val dislikes: Long,
        val uploaderVerified: Boolean,
        val channelName: String,
        val channelUrl: String,
        val channelAvatarUrl: String,
        val views: Long,
        val relatedItems: List<InfoItem>,
        val thumbnailUrl: String,
        val frames: List<Frameset>,
        val hlsManifestUrl: String,
        val streamType: String
)
