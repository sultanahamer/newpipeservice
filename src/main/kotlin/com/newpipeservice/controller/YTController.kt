package com.newpipeservice.controller

import com.newpipeservice.model.YTVideoDetails
import com.newpipeservice.service.YTService
import org.schabi.newpipe.extractor.InfoItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RequestBody

@Controller
class YTController {
    @Autowired
    lateinit var ytService: YTService

    @PostMapping("/query")
    public fun query(@RequestBody query: String?): ResponseEntity<List<InfoItem>> {
        return ResponseEntity<List<InfoItem>>(ytService.query(query ?: ""), HttpStatus.OK)
    }

    @GetMapping("/stream")
    public fun stream(@RequestParam(value="url") url: String?) : ResponseEntity<YTVideoDetails> {
        if (url == null) return ResponseEntity(HttpStatus.BAD_REQUEST)
        return ResponseEntity(ytService.getStreamDetails(url), HttpStatus.OK)
    }

    @GetMapping("/channel")
    public fun channel(@RequestParam(value="url") url: String?): ResponseEntity<List<InfoItem>> {
        if (url == null) return ResponseEntity(HttpStatus.BAD_REQUEST)
        return ResponseEntity<List<InfoItem>>(ytService.videosOfChannel(url), HttpStatus.OK)
    }

    @GetMapping("/feed")
    public fun channel(): ResponseEntity<List<InfoItem>> {
        return ResponseEntity<List<InfoItem>>(ytService.feed(), HttpStatus.OK)
    }

   // @GetMapping("/comments")
   // public fun comments(@RequestParam(value="url") url: String?): ResponseEntity<List<InfoItem>> {
   //
   // }
}
