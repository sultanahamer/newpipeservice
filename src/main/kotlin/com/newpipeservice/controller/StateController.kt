package com.newpipeservice.controller

import org.springframework.http.HttpStatus.*
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import java.io.BufferedReader
import java.io.File

@Controller
class StateController {
    val storePath: String = System.getenv("STOREPATH") ?: ""
    fun isValidStorePath(): Boolean {
        if(storePath == "") return false
        return File(storePath).exists()
    }
    @GetMapping("/store")
    fun getStore(): ResponseEntity<String> {
        if (!isValidStorePath()) return ResponseEntity("{}", OK)
        val bufferedReader: BufferedReader = File(storePath).bufferedReader()
        return ResponseEntity(bufferedReader.use { it.readText() }, OK)
    }
    @PostMapping("/store")
    fun updateStore(@RequestBody text: String): ResponseEntity<String> {
        if (!isValidStorePath()) return ResponseEntity("{}", OK)
        File(storePath).writeText(text)
        return ResponseEntity(OK)
    }
}
