package com.newpipeservice.controller

import com.newpipeservice.ConfigUtils
import com.newpipeservice.encodeUri
import org.apache.tomcat.util.http.fileupload.util.Streams
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import java.net.URI
import java.net.URL
import javax.servlet.http.HttpServletResponse


@Controller
class ProxyController {
    @Autowired
    lateinit var config: ConfigUtils
    val hlsProxyEndpoint = "proxyHLS"
    @GetMapping("/proxy")
    fun get(@RequestParam url: String, response: HttpServletResponse) {
        val responseFromActualSite = RestTemplateBuilder()
            .interceptors()
            .build()
            .requestFactory.createRequest(URI.create(url), HttpMethod.GET)
            .execute()
        val responseStream = responseFromActualSite
            .body
        responseFromActualSite.headers.forEach{response.addHeader(it.key, it.value.toString())}
        Streams.copy(responseStream, response.outputStream, false, ByteArray(1024))
    }

    @GetMapping("proxyHLS")
    fun proxyManifest(@RequestParam url: String): ResponseEntity<String> {
        if(!config.shouldProxyMedia()) return ResponseEntity.unprocessableEntity().build()
        val proxifyUrl: (String) -> String = { toBeProxiedUrl ->
            if(toBeProxiedUrl.contains("manifest.googlevideo")) "http://localhost:6060/$hlsProxyEndpoint?url=${encodeUri(toBeProxiedUrl)}"
            else "http://localhost:6060/proxy?url=${encodeUri(toBeProxiedUrl)}"
        }

        val proxifyManifest: (String) -> String = { line: String ->
            val urlRegex = Regex("https.*")
            val m3u8Regex = Regex("https.*m3u8")
            if(line.contains("#EXT-X-MEDIA:URI")) {
                val treatedURL = proxifyUrl(m3u8Regex.find(line)!!.value)
                line.replace(m3u8Regex, treatedURL)
            }
            else if (line.contains("https"))  {
                val treatedURL = proxifyUrl(urlRegex.find(line)!!.value)
                line.replace(urlRegex, treatedURL)
            }
            else line
        }
        val content = replaceContent(url, proxifyManifest)
        return ResponseEntity<String>(content, HttpStatus.OK)
    }



    private fun replaceContent(url: String, forEachLine: (line: String) -> String): String {
        val message = String(URL(url).openStream().readAllBytes())
        return message.split("\n").map(forEachLine).joinToString("\n")
    }

}
