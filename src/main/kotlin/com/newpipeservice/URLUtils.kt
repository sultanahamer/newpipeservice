package com.newpipeservice

import java.net.URLEncoder
import java.nio.charset.StandardCharsets

fun encodeUri(line: String): String = URLEncoder.encode(line, StandardCharsets.UTF_8.toString())
