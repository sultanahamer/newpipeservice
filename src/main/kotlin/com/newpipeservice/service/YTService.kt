package com.newpipeservice.service

import com.newpipeservice.encodeUri
import com.newpipeservice.model.YTVideoDetails
import org.schabi.newpipe.DownloaderImpl
import org.schabi.newpipe.extractor.InfoItem
import org.schabi.newpipe.extractor.MediaFormat
import org.schabi.newpipe.extractor.NewPipe
import org.schabi.newpipe.extractor.ServiceList
import org.schabi.newpipe.extractor.channel.tabs.ChannelTabs
import org.schabi.newpipe.extractor.comments.CommentsInfoItem
import org.schabi.newpipe.extractor.services.youtube.YoutubeService
import org.schabi.newpipe.extractor.stream.StreamExtractor
import org.springframework.stereotype.Service
import java.net.URI

@Service
class YTService {
    
    var youtubeService: YoutubeService
    constructor() {
        NewPipe.init(DownloaderImpl.init(null))
        youtubeService = ServiceList.YouTube;
    }

    public fun query(queryText: String): List<InfoItem> {
        val searchExtractor = youtubeService.getSearchExtractor(queryText)
        searchExtractor.fetchPage()
        return searchExtractor.initialPage.items
    }

    public fun videosOfChannel(channelURL: String): List<InfoItem> {
        val tabExtractor = youtubeService.getChannelTabExtractorFromId(URI.create(channelURL).path, ChannelTabs.VIDEOS)
        tabExtractor.fetchPage()
        tabExtractor.initialPage
        return tabExtractor.initialPage.items
    }

    public fun feed(): List<InfoItem> {
        youtubeService.kioskList.availableKiosks
        val defaultKioskExtractor = youtubeService.kioskList.defaultKioskExtractor
        defaultKioskExtractor.fetchPage()
        return defaultKioskExtractor.initialPage.items
    }

    private fun getDislikeCount(streamExtractor: StreamExtractor): Long { // TODO: like and dislike counts are failing
    // url to try for failures is https://www.youtube.com/watch?v=qs3ejsrnQUQ
      try {
        return streamExtractor.dislikeCount;
      } catch(e: Exception){
        e.printStackTrace();
        return 0L;
      }
    }

    private fun getLikeCount(streamExtractor: StreamExtractor): Long { // TODO: like and dislike counts are failing
    // url to try for failures is https://www.youtube.com/watch?v=qs3ejsrnQUQ
      try {
        return streamExtractor.likeCount;
      } catch(e: Exception){
        e.printStackTrace();
        return 0L;
      }
    }

    public fun getStreamDetails(ytVideoUrl: String): YTVideoDetails {
        val streamExtractor = youtubeService.getStreamExtractor(ytVideoUrl)
        streamExtractor.fetchPage()
        return YTVideoDetails(
                streamExtractor.name,
                streamExtractor.description.content,
                streamExtractor.videoStreams,
                streamExtractor.videoOnlyStreams,
                streamExtractor.audioStreams,
                streamExtractor.getSubtitles(MediaFormat.VTT),
                streamExtractor.textualUploadDate,
                getLikeCount(streamExtractor),
                getDislikeCount(streamExtractor),
                streamExtractor.isUploaderVerified,
                streamExtractor.uploaderName,
                streamExtractor.uploaderUrl,
                streamExtractor.uploaderAvatars.first().url,
                streamExtractor.viewCount,
     streamExtractor.relatedItems?.items ?: emptyList(),
                streamExtractor.thumbnails.first().url,
                streamExtractor.frames,
                encodeUri(streamExtractor.hlsUrl),
                streamExtractor.streamType.name
        )
    }

//    public fun getComments(ytVideoUrl: String, page: Int = 0): List<CommentsInfoItem>? {
//        val commentsExtractor = youtubeService.getCommentsExtractor(ytVideoUrl)
//        commentsExtractor.fetchPage()
//        return commentsExtractor.initialPage.items
//    }
}
