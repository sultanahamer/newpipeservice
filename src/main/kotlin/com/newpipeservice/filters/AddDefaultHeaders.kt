package com.newpipeservice.filters

import org.springframework.stereotype.Component
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.annotation.WebFilter
import javax.servlet.http.HttpServletResponse

@WebFilter("/*")
@Component
class AddDefaultHeaders : Filter {
    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
        (response as HttpServletResponse).addHeader("Access-Control-Allow-Origin", "*")
        chain?.doFilter(request, response);
    }

}