package com.newpipeservice

import kotlin.collections.HashSet

object CookieUtils {
    fun concatCookies(cookieStrings: Collection<String>): String {
        val cookieSet: MutableSet<String> = HashSet()
        for (cookies in cookieStrings) {
            cookieSet.addAll(splitCookies(cookies))
        }
        return cookieSet.joinToString { "; " }.trim()
    }

    fun splitCookies(cookies: String): Set<String> {
        return HashSet(listOf(*cookies.split("; *").toTypedArray()))
    }
}