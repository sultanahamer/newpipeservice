package org.schabi.newpipe

import com.newpipeservice.CookieUtils
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.schabi.newpipe.extractor.downloader.Downloader
import org.schabi.newpipe.extractor.downloader.Request
import org.schabi.newpipe.extractor.downloader.Response
import org.schabi.newpipe.extractor.exceptions.ReCaptchaException
import org.springframework.lang.NonNull
import org.springframework.lang.Nullable
import java.io.IOException
import java.io.InputStream
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class DownloaderImpl private constructor(builder: OkHttpClient.Builder) : Downloader() {
    val RECAPTCHA_COOKIES_KEY: String = "recaptcha_cookies"
    private val mCookies: MutableMap<String, String>
    private val client: OkHttpClient
    fun getCookies(url: String): String {
        val resultCookies: MutableList<String> = ArrayList()
        if (url.contains(YOUTUBE_DOMAIN)) {
            val youtubeCookie = getCookie(YOUTUBE_RESTRICTED_MODE_COOKIE_KEY)
            if (youtubeCookie != null) {
                resultCookies.add(youtubeCookie)
            }
        }
        // Recaptcha cookie is always added TODO: not sure if this is necessary
        val recaptchaCookie = getCookie(RECAPTCHA_COOKIES_KEY)
        if (recaptchaCookie != null) {
            resultCookies.add(recaptchaCookie)
        }
        return CookieUtils.concatCookies(resultCookies)
    }

    fun getCookie(key: String): String? {
        return mCookies[key]
    }

    fun setCookie(key: String, cookie: String) {
        mCookies[key] = cookie
    }

    fun removeCookie(key: String) {
        mCookies.remove(key)
    }

//    fun updateYoutubeRestrictedModeCookies(context: Context) {
//        val restrictedModeEnabledKey: String = context.getString(R.string.youtube_restricted_mode_enabled)
//        val restrictedModeEnabled: Boolean = PreferenceManager.getDefaultSharedPreferences(context)
//            .getBoolean(restrictedModeEnabledKey, false)
//        updateYoutubeRestrictedModeCookies(restrictedModeEnabled)
//    }
//
//    fun updateYoutubeRestrictedModeCookies(youtubeRestrictedModeEnabled: Boolean) {
//        if (youtubeRestrictedModeEnabled) {
//            setCookie(
//                YOUTUBE_RESTRICTED_MODE_COOKIE_KEY,
//                YOUTUBE_RESTRICTED_MODE_COOKIE
//            )
//        } else {
//            removeCookie(YOUTUBE_RESTRICTED_MODE_COOKIE_KEY)
//        }
//        InfoCache.getInstance().clearCache()
//    }

    /**
     * Get the size of the content that the url is pointing by firing a HEAD request.
     *
     * @param url an url pointing to the content
     * @return the size of the content, in bytes
     */
    @Throws(IOException::class)
    fun getContentLength(url: String?): Long {
        return try {
            val response = head(url)
            response.getHeader("Content-Length")!!.toLong()
        } catch (e: NumberFormatException) {
            throw IOException("Invalid content length", e)
        } catch (e: ReCaptchaException) {
            throw IOException(e)
        }
    }

    @Throws(IOException::class)
    fun stream(siteUrl: String): InputStream? {
        return try {
            val requestBuilder: okhttp3.Request.Builder = okhttp3.Request.Builder()
                .method("GET", null).url(siteUrl)
                .addHeader("User-Agent", USER_AGENT)
            val cookies = getCookies(siteUrl)
            if (cookies.isNotEmpty()) {
                requestBuilder.addHeader("Cookie", cookies)
            }
            val request: okhttp3.Request = requestBuilder.build()
            val response: okhttp3.Response = client.newCall(request).execute()
            val body: ResponseBody? = response.body
            if (response.code === 429) {
                throw ReCaptchaException("reCaptcha Challenge requested", siteUrl)
            }
            if (body == null) {
                response.close()
                return null
            }
            body.byteStream()
        } catch (e: ReCaptchaException) {
            throw IOException(e.message, e.cause)
        }
    }

    @Throws(IOException::class, ReCaptchaException::class)
    override fun execute(@NonNull request: Request): Response {
        val httpMethod = request.httpMethod()
        val url = request.url()
        val headers = request.headers()
        val dataToSend = request.dataToSend()
        var requestBody: RequestBody? = null
        if (dataToSend != null) {
            requestBody = RequestBody.create(null, dataToSend)
        }
        val requestBuilder: okhttp3.Request.Builder = okhttp3.Request.Builder()
            .method(httpMethod, requestBody).url(url)
            .addHeader("User-Agent", USER_AGENT)
        val cookies = getCookies(url)
        if (!cookies.isEmpty()) {
            requestBuilder.addHeader("Cookie", cookies)
        }
        for ((headerName, headerValueList) in headers) {
            if (headerValueList.size > 1) {
                requestBuilder.removeHeader(headerName)
                for (headerValue in headerValueList) {
                    requestBuilder.addHeader(headerName, headerValue)
                }
            } else if (headerValueList.size == 1) {
                requestBuilder.header(headerName, headerValueList[0])
            }
        }
        val response: okhttp3.Response = client.newCall(requestBuilder.build()).execute()
        if (response.code === 429) {
            response.close()
            throw ReCaptchaException("reCaptcha Challenge requested", url)
        }
        val body: ResponseBody? = response.body
        var responseBodyToReturn: String? = null
        if (body != null) {
            responseBodyToReturn = body.string()
        }
        val latestUrl: String = response.request.url.toString()
        return Response(
            response.code, response.message, response.headers.toMultimap(),
            responseBodyToReturn, latestUrl
        )
    }

    companion object {
        const val USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:68.0) Gecko/20100101 Firefox/68.0"
        const val YOUTUBE_RESTRICTED_MODE_COOKIE_KEY = "youtube_restricted_mode_key"
        const val YOUTUBE_RESTRICTED_MODE_COOKIE = "PREF=f2=8000000"
        const val YOUTUBE_DOMAIN = "youtube.com"
        var instance: DownloaderImpl? = null
            private set

        /**
         * It's recommended to call exactly once in the entire lifetime of the application.
         *
         * @param builder if null, default builder will be used
         * @return a new instance of [DownloaderImpl]
         */
        fun init(@Nullable builder: OkHttpClient.Builder?): DownloaderImpl? {
            instance = DownloaderImpl(
                builder ?: OkHttpClient.Builder()
            )
            return instance
        }

    }

    init {

        client = builder
            .readTimeout(
                30,
                TimeUnit.SECONDS
            ) //                .cache(new Cache(new File(context.getExternalCacheDir(), "okhttp"),
            //                        16 * 1024 * 1024))
            .build()
        mCookies = HashMap()
    }
}