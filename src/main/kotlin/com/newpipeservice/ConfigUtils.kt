package com.newpipeservice

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class ConfigUtils {
    @Value("\${proxy.should_proxy_media}")
    lateinit var shouldProxyMediaConfig: String

    val shouldProxyMedia =  { shouldProxyMediaConfig.lowercase() == "true" }

}
